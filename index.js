const express = require('express');
const cors = require('cors');

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({ path: '.env.local' });
}

const DB = require('./database');

const app = express();
const port = process.env.PORT || 9000;

app.use(cors());

app.get('/data', (req, res) => {
  DB.query('SELECT name,age FROM gabe_test;', data => {
    res.json(data);
  });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
