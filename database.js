const mysql = require('mysql');

const DB = {
  query: (sql, cb) => {
    const conn = mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_SCHEMA
    });

    conn.connect();

    conn.query(sql, function (error, results, fields) {
      if (error) throw error;
      cb(results);
    });

    conn.end();
  }
};

module.exports = DB;
